# frozen_string_literal: true

require_relative 'devops_labels'

module LabelsHelper
  include DevopsLabels::Context

  # Returns a time when the given `label` was added.
  # Returns `nil` if the label isn't set on the resource.
  def label_added_at(label)
    labels_with_details.find { |label_with_details| label_with_details.name == label }&.added_at
  end

  # Returns true when the given `label` was added before the given `time`.
  # Returns false otherwise, or if the label isn't set on the resource.
  def label_added_before?(label, time)
    added_at = label_added_at(label)
    return false unless added_at

    added_at <= time
  end

  def feature_label?
    # TODO: Label updates to be handled in https://gitlab.com/gitlab-org/quality/team-tasks/-/issues/1046
    label_names.include?('type::feature')
  end

  def p1_bug_label?
    # TODO: Label updates to be handled in https://gitlab.com/gitlab-org/quality/team-tasks/-/issues/1046
    bug_labels = ['type::bug', 'priority::1']
    bug_labels.all? { |label| label_names.include?(label) }
  end
end
