# frozen_string_literal: true

require 'yaml'

require_relative '../../triage/processor'
require_relative '../../../lib/constants/labels'
require_relative '../change/feature_flag_default_enabled_false_change'
require_relative '../change/feature_flag_default_enabled_true_change'

module Triage
  class NotifyFeatureFlagMrDeployment < Processor
    INTERESTED_CHANGES = [
      FeatureFlagDefaultEnabledTrueChange,
      FeatureFlagDefaultEnabledFalseChange
    ].freeze
    ENVIRONMENT_LABEL_TO_ENVIRONMENT_ARG_NAME = {
      Labels::WORKFLOW_STAGING_CANARY => 'staging',
      Labels::WORKFLOW_STAGING => 'staging',
      Labels::WORKFLOW_STAGING_REF => 'staging-ref',
      Labels::WORKFLOW_CANARY => 'production',
      Labels::WORKFLOW_PRODUCTION => 'production'
    }.freeze

    attr_reader :processor

    def initialize(new_processor)
      @processor = new_processor
    end

    def applicable?
      processor.event.from_gitlab_org_gitlab? &&
        processor.event.resource_merged? &&
        environment_label_added? &&
        changes_should_notify.any?
    end

    def react
      message =
        if feature_flag_change.type == 'FeatureFlagDefaultEnabledTrueChange'
          notify_feature_flag_default_enabled_true_deployed(mention: !discussion_thread)
        else
          notify_feature_flag_default_enabled_false_deployed(mention: !discussion_thread)
        end

      if discussion_thread
        processor.append_discussion(message, discussion_thread['id'], append_source_link: true)
      else
        processor.add_discussion(message, append_source_link: true)
      end
    end

    def documentation
      <<~MARKDOWN
        This processor reminds the MR author to enable or delete the feature flag when their MR adding/updating a feature flag has ~"workflow::<environment>" label added.
      MARKDOWN
    end

    private

    def environment_label_added?
      !!environment_label_added
    end

    def environment_label_added
      (processor.event.added_label_names & Labels::WORKFLOW_ENVIRONMENTS).first
    end

    def notify_feature_flag_default_enabled_true_deployed(mention:)
      notification do
        <<~MARKDOWN.chomp
          :rocket: #{mention_author_in_sentence(mention: mention)}This merge request was deployed to the ~"#{environment_label_added}" environment.
          You may want to delete the associated feature flag record on this environment with `/chatops run feature delete #{feature_flag_name} --dev --pre --#{environment_arg_name}`.
        MARKDOWN
      end
    end

    def notify_feature_flag_default_enabled_false_deployed(mention:)
      notification do
        <<~MARKDOWN.chomp
          :rocket: #{mention_author_in_sentence(mention: mention)}This merge request was deployed to the ~"#{environment_label_added}" environment.
          You may want to enable the associated feature flag on this environment with `/chatops run feature set #{feature_flag_name} true --#{environment_arg_name}`.
        MARKDOWN
      end
    end

    def mention_author_in_sentence(mention:)
      return '' unless mention

      "@#{processor.event.resource_author.username} "
    end

    def feature_flag_change
      @feature_flag_change ||= begin
        raise 'Cannot notify for both `default_enabled: true` and `default: enabled: false`!' unless changes_should_notify.one?

        changes_should_notify.first
      end
    end

    def feature_flag_name
      feature_flag_definition.fetch(:name, '<could not retrieve ff name>')
    end

    def environment_arg_name
      ENVIRONMENT_LABEL_TO_ENVIRONMENT_ARG_NAME[environment_label_added]
    end

    def feature_flag_definition
      @feature_flag_definition ||= begin
        file_content = Triage.api_client.file_contents(processor.event.project_id, feature_flag_change.path)

        if file_content
          YAML.safe_load(
            file_content,
            permitted_classes: [Symbol],
            symbolize_names: true
          )
        else
          {}
        end
      end
    end

    def notification
      unique_comment.wrap(yield.strip)
    end

    def discussion_thread
      return @discussion_thread if defined?(@discussion_thread)

      @discussion_thread = unique_comment.previous_discussion
    end

    def changes_should_notify
      @changes_should_notify ||= INTERESTED_CHANGES.flat_map do |change|
        change.detect(processor.changed_file_list)
      end
    end

    def unique_comment
      @unique_comment ||= UniqueComment.new('Triage::NotifyFeatureFlagMrDeployment', processor.event)
    end
  end
end
