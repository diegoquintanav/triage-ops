# frozen_string_literal: true

require_relative '../../../../lib/constants/labels'

module Triage
  module PipelineFailure
    module Config
      class Base
        DEFAULT_AUTO_TRIAGE_FLAG = false
        DEFAULT_INCIDENT_SUMMARY_TABLE = <<~MARKDOWN
          ## %<project_link>s pipeline %<pipeline_link>s failed

          | Pipeline ID | Branch | Commit | Merge request | Source | Duration | Triggered by |
          | ----------- | ------ | ------ | ------------- | ------ | -------- | ------------ |
          | `%<pipeline_id>s` | %<branch_link>s | %<commit_link>s | %<merge_request_link>s | `%<pipeline_source>s` | %<pipeline_duration>s minutes | %<triggered_by_link>s |

          **Failed jobs (%<failed_jobs_count>s):**

          %<failed_jobs_list>s
        MARKDOWN
        DEFAULT_SLACK_ICON = ':boom:'
        DEFAULT_SLACK_USERNAME = GITLAB_BOT
        DEFAULT_SLACK_PAYLOAD_TEMPLATE = <<~JSON
          {
            "blocks": [
              {
                "type": "section",
                "text": {
                  "type": "mrkdwn",
                  "text": "*%<title>s*"
                },
                "accessory": {
                  "type": "button",
                  "text": {
                    "type": "plain_text",
                    "text": "%<incident_button_text>s"
                  },
                  "url": "%<incident_button_link>s"
                }
              },
              {
                "type": "section",
                "text": {
                  "type": "mrkdwn",
                  "text": "*Branch*: %<branch_link>s"
                }
              },
              {
                "type": "section",
                "text": {
                  "type": "mrkdwn",
                  "text": "*Commit*: %<commit_link>s"
                }
              },
              {
                "type": "section",
                "text": {
                  "type": "mrkdwn",
                  "text": "*Triggered by* %<triggered_by_link>s • *Source:* %<pipeline_source>s • *Duration:* %<pipeline_duration>s minutes"
                }
              },
              {
                "type": "section",
                "text": {
                  "type": "mrkdwn",
                  "text": "*Failed jobs (%<failed_jobs_count>s):* %<failed_jobs_list>s"
                }
              }
              <% if incident %>
              ,{
                "type": "section",
                "text": {
                  "type": "mrkdwn",
                  "text": "*Incident state:* %<incident_state>s • *Incident labels:* %<incident_labels_list>s"
                }
              }
              <% end %>
            ]
          }
        JSON
        DEFAULT_DUPLICATE_COMMAND_BODY = <<~MARKDOWN.chomp
          Closing as a duplicate of incident %<duplicate_incident_url>s. Please reopen it if you don't think this is a duplicate.

          /copy_metadata %<duplicate_incident_url>s
          /duplicate %<duplicate_incident_url>s
        MARKDOWN
        DEFAULT_CLOSE_COMMAND_BODY = <<~MARKDOWN.chomp
          This incident is caused by known transient error(s), closing.

          /close
        MARKDOWN

        def self.match?(event)
          raise NotImplementedError, "#{name} must implement `.#{__method__}`!"
        end

        def initialize(event)
          @event = event
        end

        def create_incident?
          incident_project_id && incident_template
        end

        def incident_project_id
          nil
        end

        def canonical_incident_project_id
          incident_project_id
        end

        def incident_template
          nil
        end

        def incident_labels
          []
        end

        def incident_extra_attrs
          {}
        end

        def default_slack_channels
          []
        end

        def slack_username
          DEFAULT_SLACK_USERNAME
        end

        def slack_icon
          DEFAULT_SLACK_ICON
        end

        def slack_payload_template
          DEFAULT_SLACK_PAYLOAD_TEMPLATE
        end

        def slack_options
          {
            username: slack_username,
            icon_emoji: slack_icon
          }
        end

        def auto_triage?
          DEFAULT_AUTO_TRIAGE_FLAG
        end

        def transient_root_cause_labels
          Labels::MASTER_BROKEN_ROOT_CAUSE_LABELS.values_at(
            :failed_to_pull_image,
            :gitlab_com_overloaded,
            :runner_disk_full,
            :infrastructure,
            :job_timeout
          ).freeze
        end

        def duplicate_command_body
          DEFAULT_DUPLICATE_COMMAND_BODY
        end

        def close_command_body
          DEFAULT_CLOSE_COMMAND_BODY
        end

        private

        attr_reader :event
      end
    end
  end
end
