# frozen_string_literal: true

require 'slack-messenger'

require_relative '../../triage/processor'

module Triage
  module Workflow
    class NotifyMergedMasterBrokenMergeRequest < Processor
      SLACK_CHANNEL = '#master-broken'
      SLACK_ICON = ':merge:'
      SLACK_PAYLOAD_TEMPLATE = <<~MESSAGE
        A `~master:broken` MR was *%<action>s* [%<title>s](%<url>s)
      MESSAGE

      ACTIONS = {
        open: 'opened',
        merge: 'merged',
        approved: 'approved'
      }.freeze
      react_to 'merge_request.merge', 'merge_request.open', 'merge_request.approved'

      def initialize(event, messenger: slack_messenger)
        super(event)
        @messenger = messenger
      end

      def applicable?
        event.from_gitlab_org_gitlab? && master_broken_label_set?
      end

      def process
        post_merged_mr_notification
      end

      def documentation
        <<~TEXT
          Send a notification in #master-broken channel when a Merge Request with '~master:broken'label or
          '~master:foss-broken' is merged.
        TEXT
      end

      def slack_options
        {
          channel: SLACK_CHANNEL,
          username: GITLAB_BOT,
          icon_emoji: SLACK_ICON
        }
      end

      private

      attr_reader :messenger

      def master_broken_label_set?
        (event.label_names & [Labels::MASTER_BROKEN_LABEL, Labels::MASTER_FOSS_BROKEN_LABEL]).any?
      end

      def post_merged_mr_notification
        message = format(SLACK_PAYLOAD_TEMPLATE, title: event.title, url: event.url, action: ACTIONS[event.action.to_sym])

        messenger.ping(text: message)
      end

      def slack_messenger
        Slack::Messenger.new(ENV.fetch('SLACK_WEBHOOK_URL', nil), slack_options)
      end
    end
  end
end
