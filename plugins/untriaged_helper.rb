# frozen_string_literal: true

require_relative '../lib/untriaged_helper'

Gitlab::Triage::Resource::Context.include UntriagedHelper
