# frozen_string_literal: true

require_relative '../lib/lazy_heat_map'

Gitlab::Triage::EntityBuilders::SummaryBuilder.prepend(SummaryBuilderWithHeatMap)
