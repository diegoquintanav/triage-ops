# Yamllint of *.yml for policy files.
# This uses rules from project root `.yamllint`.
lint-policies:
  stage: test
  image: pipelinecomponents/yamllint:latest
  needs: []
  script:
    - yamllint -f colored .

# Rubocop of *.rb files in the project
# This uses rules from project root `.rubocop.yml`.
lint-ruby:
  extends: .ruby-before_script
  stage: test
  needs: []
  script:
    - bundle exec rubocop -P -E .

.test-base:
  stage: test
  extends: .ruby-before_script
  needs: []
  rules:
    # We don't run specs for scheduled pipelines unless it has $SCHEDULED_TESTS set.
    - if: '$CI_PIPELINE_SOURCE == "schedule" && $SCHEDULED_TESTS == null'
      when: never
    - if: '$CI_MERGE_REQUEST_LABELS =~ /one-off/'
      when: never
    - when: on_success

regeneration-check:
  extends:
    - .test-base
  script:
    - .gitlab/ci/scripts/regeneration-check

regenerate-policies:
  extends: .ruby-before_script
  variables:
    MERGE_REQUEST_REPO: "https://oauth2:$GIT_PUSH_TOKEN@$CI_SERVER_HOST/$CI_PROJECT_PATH.git"
    MERGE_REQUEST_BRANCH: "regenerate-stale-policies"
    MERGE_REQUEST_TITLE: "Regenerate stale policies"
    MERGE_REQUEST_MENTION: "@gl-quality/eng-prod"
    MERGE_REQUEST_LABELS: "/label ~maintenance::workflow ~automation:bot-authored"
    MERGE_REQUEST_DESCRIPTION: "$MERGE_REQUEST_TITLE.\\n\\n$MERGE_REQUEST_MENTION Please review and merge :pray:\\n\\n$MERGE_REQUEST_LABELS"
  needs: []
  script:
    - |
      set -x
      git switch "$MERGE_REQUEST_BRANCH" || git switch --create "$MERGE_REQUEST_BRANCH"
      if .gitlab/ci/scripts/regeneration-check; then
        echo "No changes detected. Skipping."
        exit 0
      else
        git restore .bundle/config
        git config --global user.email "$GITLAB_USER_EMAIL"
        git config --global user.name "$GITLAB_USER_NAME"
        git add .
        git commit --all --message "$MERGE_REQUEST_TITLE"

        git push "$MERGE_REQUEST_REPO" "$MERGE_REQUEST_BRANCH:$MERGE_REQUEST_BRANCH" \
          -o merge_request.create \
          -o merge_request.title="$MERGE_REQUEST_TITLE" \
          -o merge_request.description="$MERGE_REQUEST_DESCRIPTION"
      fi
  rules:
    - if: $REGENERATE_POLICIES == '1'
    - if: '$CI_MERGE_REQUEST_IID'
      changes:
        - ".gitlab/ci/test.yml"
      when: manual
      allow_failure: true

specs:
  extends:
    - .test-base
  variables:
    BUNDLE_WITH: "coverage"
  script:
    - bundle exec rspec -Ispec -rspec_helper --color --format documentation --format RspecJunitFormatter --out ${JUNIT_RESULT_FILE}
  coverage: '/LOC \((\d+\.\d+%)\) covered.$/'
  artifacts:
    expire_in: 7d
    when: always
    paths:
      - coverage/
      - rspec/
    reports:
      junit: ${JUNIT_RESULT_FILE}
      coverage_report:
        coverage_format: cobertura
        path: ${COVERAGE_FILE}

undercoverage:
  extends:
    - .test-base
  rules:
    - if: '$CI_MERGE_REQUEST_LABELS =~ /pipeline:skip-undercoverage/'
      when: never
    - if: '$CI_MERGE_REQUEST_LABELS =~ /one-off/'
      when: never
    - if: '$CI_MERGE_REQUEST_IID'
  variables:
    BUNDLE_WITH: "coverage"
  needs: ["specs"]
  script:
    - git fetch ${CI_MERGE_REQUEST_SOURCE_PROJECT_URL} ${CI_MERGE_REQUEST_SOURCE_BRANCH_NAME};
    - if [ -n "$CI_MERGE_REQUEST_SOURCE_BRANCH_SHA" ]; then
        echo "Checking out \$CI_MERGE_REQUEST_SOURCE_BRANCH_SHA ($CI_MERGE_REQUEST_SOURCE_BRANCH_SHA) instead of \$CI_COMMIT_SHA (merge result commit $CI_COMMIT_SHA) so we can use $CI_MERGE_REQUEST_DIFF_BASE_SHA for undercoverage in this merged result pipeline";
        git checkout -f ${CI_MERGE_REQUEST_SOURCE_BRANCH_SHA};
        bundle_install_script;
      else
        echo "Using \$CI_COMMIT_SHA ($CI_COMMIT_SHA) for this non-merge result pipeline.";
      fi;
    - UNDERCOVERAGE_COMPARE="${CI_MERGE_REQUEST_DIFF_BASE_SHA:-$(git merge-base origin/${CI_DEFAULT_BRANCH} HEAD)}"
    - echo "Undercoverage comparing with ${UNDERCOVERAGE_COMPARE}"
    - bundle exec undercover -c "${UNDERCOVERAGE_COMPARE:-$(git merge-base origin/${CI_DEFAULT_BRANCH} HEAD)}"
  artifacts:
    expire_in: 7d
    when: always
    paths:
      - coverage/
