# frozen_string_literal: true

require 'spec_helper'
require_relative '../../../lib/shared/current_group_helper'
require_relative '../../../lib/hierarchy/group'

RSpec.describe CurrentGroupHelper do
  let(:resource_klass) do
    Struct.new(:current_group, :label_names) do
      include CurrentGroupHelper
    end
  end

  let(:label_names) { ['frontend'] }
  let(:current_group) { Hierarchy::Group.new('group1') }
  let(:expected_frontend_em)  { '@andr3' }
  let(:expected_backend_em)   { '@sean_carroll' }
  let(:team_data)             { {} }

  subject { resource_klass.new(current_group, label_names) }

  describe '#current_group' do
    it 'returns ResourceGroup object for the current group' do
      expect(subject.current_group).to eq(current_group)
    end
  end

  describe '#current_group_em' do
    context 'when frontend label is set' do
      it 'delegates to ResourceGroup#engineering_manager' do
        expect(subject.current_group_em).to eq('@fake_fe_em')
      end
    end

    context 'when frontend label is not set' do
      let(:label_names) { [] }

      it 'delegates to ResourceGroup#engineering_manager' do
        expect(subject.current_group_em).to eq('@fake_be_em')
      end
    end

    context 'when only engineering_managers is defined' do
      let(:current_group) { Hierarchy::Group.new('engineering_productivity') }

      context 'with frontend label' do
        it 'returns the name of the generic em' do
          expect(subject.current_group_em).to eq('@dcroft')
        end
      end

      context 'with backend label' do
        let(:label_names)   { ['backend'] }

        it 'returns the name of the generic em' do
          expect(subject.current_group_em).to eq('@dcroft')
        end
      end
    end

    context 'when only fullstack_managers is defined' do
      let(:current_group) { Hierarchy::Group.new('threat_insights') }

      context 'with frontend label' do
        it 'returns the name of the fullstack em' do
          expect(subject.current_group_em).to eq('@kniechajewicz')
        end
      end

      context 'with backend label' do
        let(:label_names)   { ['backend'] }

        it 'returns the name of the fullstack em' do
          expect(subject.current_group_em).to eq('@kniechajewicz')
        end
      end
    end
  end

  describe '#current_group_set' do
    describe 'SET role detection' do
      it 'detects SET' do
        expect(subject.current_group_set).to eq('@p-4')
      end
    end

    describe 'SET speciality detection' do
      let(:current_group) { Hierarchy::Group.new('group2') }

      it 'detects SET' do
        expect(subject.current_group_set).to eq('@p-5')
      end
    end
  end

  describe '#current_group_pm' do
    it 'delegates to ResourceGroup#product_manager' do
      expect(subject.current_group_pm).to eq('@fake_pm')
    end
  end
end
