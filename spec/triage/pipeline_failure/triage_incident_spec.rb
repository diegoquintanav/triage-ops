# frozen_string_literal: true

require 'spec_helper'
require 'tempfile'

require_relative '../../../triage/triage/event'
require_relative '../../../triage/triage/pipeline_failure/triage_incident'
require_relative '../../../triage/triage/pipeline_failure/config/master_branch'

RSpec.describe Triage::PipelineFailure::TriageIncident do
  def stub_ci_job(job_id: 999, **attrs)
    name = "job #{job_id}"
    web_url = "https://gitlab.com/group/project/-/jobs/#{job_id}"
    default_attrs = {
      job_id: job_id,
      name: "job #{job_id}",
      web_url: web_url,
      markdown_link: "[#{name}](#{web_url})",
      failure_root_cause_label: 'master-broken::undetermined',
      test_failure_summary_markdown: '',
      attribution_message_markdown: '',
      rspec_run_time_summary_markdown: '',
      potential_responsible_group_labels: []
    }
    instance_double(Triage::CiJob, default_attrs.merge(attrs))
  end

  def duplicate_jobs(template_job, count, **attrs)
    (1..count).map do
      stub_ci_job(name: template_job.name, web_url: template_job.web_url, instance: :dev, **attrs)
    end
  end

  let(:job_failed_to_pull_image) { stub_ci_job(failure_root_cause_label: 'master-broken::failed-to-pull-image') }
  let(:job_infrastructure) { stub_ci_job(failure_root_cause_label: 'master-broken::infrastructure') }
  let(:job_gitlab_com_overloaded) { stub_ci_job(failure_root_cause_label: 'master-broken::gitlab-com-overloaded') }
  let(:job_undetermined) { stub_ci_job(failure_root_cause_label: 'master-broken::undetermined') }
  let(:job_runner_disk_full) { stub_ci_job(failure_root_cause_label: 'master-broken::runner-disk-full') }
  let(:job_timeout) { stub_ci_job(failure_root_cause_label: 'master-broken::job-timeout') }
  let(:job_rspec) { stub_ci_job(potential_responsible_group_labels: ['group::foundations', 'group::knowledge']) }
  let(:job_jest) { stub_ci_job(potential_responsible_group_labels: ['frontend']) }

  let(:event) { instance_double(Triage::PipelineEvent, id: 123, project_id: Triage::Event::GITLAB_PROJECT_ID, instance: :com) }
  let(:config) { Triage::PipelineFailure::Config::MasterBranch.new(event) }
  let(:ci_jobs) { [] }

  subject(:triager) { described_class.new(event: event, config: config, ci_jobs: ci_jobs) }

  describe '#top_root_cause_label' do
    context 'with no failed jobs' do
      it 'returns master-broken::undetermined' do
        expect(triager.top_root_cause_label).to eq('master-broken::undetermined')
      end
    end

    context 'with only 1 job failure which is caused by unknown reason' do
      let(:ci_jobs) { [job_undetermined] }

      it 'returns master-broken::undetermined' do
        expect(triager.top_root_cause_label).to eq('master-broken::undetermined')
      end
    end

    context 'with only 1 job failure which is caused by runner-disk-full' do
      let(:ci_jobs) { [job_runner_disk_full] }

      it 'returns master-broken::runner-disk-full' do
        expect(triager.top_root_cause_label).to eq('master-broken::runner-disk-full')
      end
    end

    context 'with mulitple jobs failed due to failed-to-pull-image' do
      let(:ci_jobs) { duplicate_jobs(job_failed_to_pull_image, 10, failure_root_cause_label: 'master-broken::failed-to-pull-image') }

      it 'returns master-broken::failed-to-pull-image' do
        expect(triager.top_root_cause_label).to eq('master-broken::failed-to-pull-image')
      end
    end

    context 'with most jobs failed due to failed-to-pull-image except 1 unknown root cause' do
      let(:ci_jobs) { duplicate_jobs(job_failed_to_pull_image, 9, failure_root_cause_label: 'master-broken::failed-to-pull-image').push(job_undetermined) }

      it 'returns master-broken::failed-to-pull-image' do
        expect(triager.top_root_cause_label).to eq('master-broken::failed-to-pull-image')
      end
    end

    context 'with some repeated transient errors and some non-repeated, non-transient errors' do
      let(:ci_jobs) { [job_failed_to_pull_image, job_failed_to_pull_image, job_infrastructure, job_gitlab_com_overloaded, job_undetermined, job_runner_disk_full, job_timeout] }

      it 'return the most repeated root cause label' do
        expect(triager.top_root_cause_label).to eq('master-broken::failed-to-pull-image')
      end
    end

    context 'when every job has a different root cause' do
      let(:ci_jobs) { [job_failed_to_pull_image, job_infrastructure, job_gitlab_com_overloaded, job_undetermined, job_runner_disk_full, job_timeout] }

      it 'returns any root cause besides master-broken::undetermined' do
        expect(triager.top_root_cause_label).not_to eq('master-broken::undetermined')
      end
    end
  end

  describe '#top_group_label' do
    context 'with no failed jobs' do
      it 'returns master-broken::undetermined' do
        expect(triager.top_group_label).to be_nil
      end
    end

    context 'with only 1 job failure which is caused by runner-disk-full' do
      let(:ci_jobs) { [job_runner_disk_full] }

      it 'returns no top_group_label' do
        expect(triager.top_group_label).to be_nil
      end
    end

    context 'with 1 jest job failure' do
      let(:ci_jobs) { [job_jest] }

      it 'returns frontend as top group label' do
        expect(triager.top_group_label).to eq('frontend')
      end
    end

    context 'with 1 jest job and 1 timeout job failure' do
      let(:ci_jobs) { [job_timeout, job_jest] }

      it 'returns the attributed group label based on the rspec job failure' do
        expect(triager.top_group_label).to eq('frontend')
      end
    end

    context 'with only 1 RSpec job failure' do
      let(:ci_jobs) { [job_rspec] }

      it 'returns the corresponding group label' do
        expect(triager.top_group_label).to eq('group::foundations')
      end
    end

    context 'with 2 RSpec job failure' do
      let(:ci_jobs) { [job_rspec, job_rspec] }

      it 'returns the corresponding group label' do
        expect(triager.top_group_label).to eq('group::foundations')
      end
    end
  end

  describe '#root_cause_analysis_comment' do
    before do
      allow(Triage.api_client).to receive(:issues).and_return([])
    end

    context 'with all job failures due to a known transient error' do
      let(:ci_jobs) { duplicate_jobs(job_failed_to_pull_image, 10, failure_root_cause_label: 'master-broken::failed-to-pull-image') }

      it 'determines root cause from trace, retries pipeline, and closes incident' do
        expect(Triage.api_client).to receive(:retry_pipeline).with(event.project_id, event.id).and_return(Gitlab::ObjectifiedHash.new({ 'web_url' => 'retried_pipeline_web_url' }))

        expect(triager.root_cause_analysis_comment).to eq(
          <<~MARKDOWN.chomp.prepend("\n\n")
            - [job 999](https://gitlab.com/group/project/-/jobs/999): ~"master-broken::failed-to-pull-image".
            - [job 999](https://gitlab.com/group/project/-/jobs/999): ~"master-broken::failed-to-pull-image".
            - [job 999](https://gitlab.com/group/project/-/jobs/999): ~"master-broken::failed-to-pull-image".
            - [job 999](https://gitlab.com/group/project/-/jobs/999): ~"master-broken::failed-to-pull-image".
            - [job 999](https://gitlab.com/group/project/-/jobs/999): ~"master-broken::failed-to-pull-image".
            - [job 999](https://gitlab.com/group/project/-/jobs/999): ~"master-broken::failed-to-pull-image".
            - [job 999](https://gitlab.com/group/project/-/jobs/999): ~"master-broken::failed-to-pull-image".
            - [job 999](https://gitlab.com/group/project/-/jobs/999): ~"master-broken::failed-to-pull-image".
            - [job 999](https://gitlab.com/group/project/-/jobs/999): ~"master-broken::failed-to-pull-image".
            - [job 999](https://gitlab.com/group/project/-/jobs/999): ~"master-broken::failed-to-pull-image".

            Retried pipeline: retried_pipeline_web_url

            This incident is caused by known transient error(s), closing.

            /close
          MARKDOWN
        )
      end
    end

    context 'with some job failures due to unknown root causes' do
      let(:ci_jobs) { duplicate_jobs(job_failed_to_pull_image, 10, failure_root_cause_label: 'master-broken::failed-to-pull-image').push(job_undetermined) }

      it 'labels each job, does not retry pipeline or close incident' do
        expect(Triage.api_client).not_to receive(:retry_pipeline)

        expect(triager.root_cause_analysis_comment).to eq(
          <<~MARKDOWN.chomp.prepend("\n\n")
            - [job 999](https://gitlab.com/group/project/-/jobs/999): ~"master-broken::failed-to-pull-image".
            - [job 999](https://gitlab.com/group/project/-/jobs/999): ~"master-broken::failed-to-pull-image".
            - [job 999](https://gitlab.com/group/project/-/jobs/999): ~"master-broken::failed-to-pull-image".
            - [job 999](https://gitlab.com/group/project/-/jobs/999): ~"master-broken::failed-to-pull-image".
            - [job 999](https://gitlab.com/group/project/-/jobs/999): ~"master-broken::failed-to-pull-image".
            - [job 999](https://gitlab.com/group/project/-/jobs/999): ~"master-broken::failed-to-pull-image".
            - [job 999](https://gitlab.com/group/project/-/jobs/999): ~"master-broken::failed-to-pull-image".
            - [job 999](https://gitlab.com/group/project/-/jobs/999): ~"master-broken::failed-to-pull-image".
            - [job 999](https://gitlab.com/group/project/-/jobs/999): ~"master-broken::failed-to-pull-image".
            - [job 999](https://gitlab.com/group/project/-/jobs/999): ~"master-broken::failed-to-pull-image".
            - [job 999](https://gitlab.com/group/project/-/jobs/999): ~"master-broken::undetermined".
          MARKDOWN
        )
      end
    end

    context 'with less than 10 failed jobs' do
      let(:ci_jobs) { duplicate_jobs(job_failed_to_pull_image, 9, failure_root_cause_label: 'master-broken::failed-to-pull-image') }

      context 'with no failed job (e.g. if the CI config is invalid)' do
        let(:ci_jobs) { [] }

        it 'returns nil' do
          expect(triager.root_cause_analysis_comment).to be_nil
        end
      end

      context 'with 1 failed job due to a known transient error' do
        let(:ci_jobs) { [job_infrastructure] }

        it 'retries the job and closes incident' do
          expect(ci_jobs).to all(receive(:retry).and_return(Gitlab::ObjectifiedHash.new({ 'web_url' => 'retry_job_web_url' })))

          expect(triager.root_cause_analysis_comment).to eq(
            <<~MARKDOWN.chomp.prepend("\n\n")
              - [job 999](https://gitlab.com/group/project/-/jobs/999): ~"master-broken::infrastructure". Retried at: retry_job_web_url

              This incident is caused by known transient error(s), closing.

              /close
            MARKDOWN
          )
        end
      end

      context 'when multiple root causes detected with some repeated causes and some non-transient errors' do
        let(:ci_jobs) { [job_gitlab_com_overloaded, job_undetermined, job_runner_disk_full] }

        it 'only retries each job failed with transient error' do
          expect(job_gitlab_com_overloaded).to receive(:retry).and_return(Gitlab::ObjectifiedHash.new({ 'web_url' => 'retry_job_web_url' }))
          expect(job_runner_disk_full).to receive(:retry).and_return(Gitlab::ObjectifiedHash.new({ 'web_url' => 'retry_job_web_url' }))
          expect(job_undetermined).not_to receive(:retry)

          expect(triager.root_cause_analysis_comment).to eq(
            <<~MARKDOWN.chomp.prepend("\n\n")
              - [job 999](https://gitlab.com/group/project/-/jobs/999): ~"master-broken::gitlab-com-overloaded". Retried at: retry_job_web_url
              - [job 999](https://gitlab.com/group/project/-/jobs/999): ~"master-broken::undetermined".
              - [job 999](https://gitlab.com/group/project/-/jobs/999): ~"master-broken::runner-disk-full". Retried at: retry_job_web_url
            MARKDOWN
          )
        end
      end

      context 'when multiple root causes detected, all caused by transient errors' do
        let(:ci_jobs) { [job_failed_to_pull_image, job_infrastructure, job_gitlab_com_overloaded, job_gitlab_com_overloaded, job_runner_disk_full] }

        it 'labels and retries each job, and closes the incident' do
          expect(ci_jobs).to all(receive(:retry).and_return(Gitlab::ObjectifiedHash.new({ 'web_url' => 'retry_job_web_url' })))

          expect(triager.root_cause_analysis_comment).to eq(
            <<~MARKDOWN.chomp.prepend("\n\n")
              - [job 999](https://gitlab.com/group/project/-/jobs/999): ~"master-broken::failed-to-pull-image". Retried at: retry_job_web_url
              - [job 999](https://gitlab.com/group/project/-/jobs/999): ~"master-broken::infrastructure". Retried at: retry_job_web_url
              - [job 999](https://gitlab.com/group/project/-/jobs/999): ~"master-broken::gitlab-com-overloaded". Retried at: retry_job_web_url
              - [job 999](https://gitlab.com/group/project/-/jobs/999): ~"master-broken::gitlab-com-overloaded". Retried at: retry_job_web_url
              - [job 999](https://gitlab.com/group/project/-/jobs/999): ~"master-broken::runner-disk-full". Retried at: retry_job_web_url

              This incident is caused by known transient error(s), closing.

              /close
            MARKDOWN
          )
        end
      end

      context 'when a workhorse job failed' do
        let(:ci_jobs) { [stub_ci_job(potential_responsible_group_labels: ['group::source code'])] }

        it 'does not retry any job and labels the job master-broken::undermined' do
          expect(triager.root_cause_analysis_comment).to eq(
            <<~MARKDOWN.chomp.prepend("\n\n")
              - [job 999](https://gitlab.com/group/project/-/jobs/999): ~"master-broken::undetermined".
            MARKDOWN
          )
        end
      end

      context 'when none of the job failures are recognized' do
        let(:ci_jobs) { duplicate_jobs(job_undetermined, 4) }

        it 'does not retry any job and labels each job master-broken::undetermined' do
          expect(triager.root_cause_analysis_comment).to eq(
            <<~MARKDOWN.chomp.prepend("\n\n")
              - [job 999](https://gitlab.com/group/project/-/jobs/999): ~"master-broken::undetermined".
              - [job 999](https://gitlab.com/group/project/-/jobs/999): ~"master-broken::undetermined".
              - [job 999](https://gitlab.com/group/project/-/jobs/999): ~"master-broken::undetermined".
              - [job 999](https://gitlab.com/group/project/-/jobs/999): ~"master-broken::undetermined".
            MARKDOWN
          )
        end
      end
    end
  end

  describe '#attribution_comment' do
    context 'with rspec failures' do
      let(:ci_jobs) { [stub_ci_job(potential_responsible_group_labels: ['group::group1'], attribution_message_markdown: '- foo bar')] }

      it 'prints the job id and feature category' do
        expect(subject.attribution_comment).to eq(
          <<~MARKDOWN.chomp.prepend("\n\n")
          - foo bar

          **This incident is attributed to ~"group::group1" and posted in `#group1`.**
          MARKDOWN
        )
      end
    end

    context 'with jest job failure' do
      let(:ci_jobs) { [stub_ci_job(potential_responsible_group_labels: ['frontend'])] }

      it 'attributes to frontend' do
        expect(subject.attribution_comment).to eq(
          <<~MARKDOWN.chomp.prepend("\n\n")
            **This incident is attributed to ~"frontend" and posted in `#frontend`.**
          MARKDOWN
        )
      end
    end

    context 'with infrastructure error' do
      let(:ci_jobs) { [stub_ci_job(potential_responsible_group_labels: ['infrastructure'])] }

      it 'attributes to infrastructure' do
        expect(subject.attribution_comment).to eq(
          <<~MARKDOWN.chomp.prepend("\n\n")
            **This incident is attributed to ~"infrastructure" and posted in `#infrastructure-lounge`.**
          MARKDOWN
        )
      end
    end

    context 'with gitlab.com overloaded error' do
      let(:ci_jobs) { [stub_ci_job(potential_responsible_group_labels: ['group::gitaly::cluster'])] }

      it 'attributes to gitaly::cluster' do
        expect(subject.attribution_comment).to eq(
          <<~MARKDOWN.chomp.prepend("\n\n")
            **This incident is attributed to ~"group::gitaly::cluster" and posted in `#g_gitaly`.**
          MARKDOWN
        )
      end
    end
  end

  describe '#investigation_comment' do
    before do
      allow(Triage.api_client).to receive(:issues).and_return([])
    end

    context 'with 1 job failed on trasient error' do
      let(:ci_jobs) { [job_failed_to_pull_image] }

      it 'returns nil' do
        expect(triager.investigation_comment).to be_nil
      end
    end

    context 'with 1 failed job showing rspec failures' do
      let(:ci_jobs) { [stub_ci_job(test_failure_summary_markdown: 'test failure summary')] }

      it 'prints test log' do
        expect(triager.investigation_comment).to eq(
          <<~MARKDOWN.chomp.prepend("\n\n")
          test failure summary
          MARKDOWN
        )
      end
    end
  end

  describe '#duration_analysis_comment' do
    let(:ci_jobs) do
      [
        stub_ci_job(rspec_run_time_summary_markdown: 'rspec_run_time_summary_markdown'),
        stub_ci_job(rspec_run_time_summary_markdown: 'another_rspec_run_time_summary_markdown'),
        stub_ci_job(rspec_run_time_summary_markdown: nil)
      ]
    end

    context 'when duration analysis comment does not exceed the RSPEC_DURATION_COMMENT_LIMIT' do
      it 'returns duration data for the job' do
        expect(triager.duration_analysis_comment).to eq(
          <<~MARKDOWN.chomp.prepend("\n\n")
          rspec_run_time_summary_markdown

          another_rspec_run_time_summary_markdown
          MARKDOWN
        )
      end
    end

    context 'when rspec_duration_comment_limit only allows showing duration data for 1 of the 3 jobs' do
      it 'returns duration data within the size limit' do
        allow(triager).to receive(:rspec_duration_comment_limit).and_return(50)

        expect(triager.duration_analysis_comment).to eq(
          <<~MARKDOWN.chomp.prepend("\n\n")
          rspec_run_time_summary_markdown
          MARKDOWN
        )
      end
    end

    context 'when rspec_duration_comment_limit is too small to show any duration analysis comment' do
      it 'does not return duration data for any job' do
        allow(triager).to receive(:rspec_duration_comment_limit).and_return(10)

        expect(triager.duration_analysis_comment).to eq('')
      end
    end
  end

  describe '#duplicate_incident_url' do
    let(:previous_incidents)  { [] }
    let(:incident_project_id) { '40549124' }
    let(:ci_jobs) { [job_failed_to_pull_image] }

    before do
      allow(Triage.api_client).to receive(:issues).with(
        incident_project_id, { order_by: 'created_at', per_page: 2, sort: "desc" }
      ).and_return(previous_incidents)
    end

    context 'when there was no previous incidents' do
      it 'returns nil' do
        expect(subject.duplicate_incident_url).to be_nil
      end
    end

    context 'when the previous incident did not fail with the same job' do
      let(:previous_incident_title) { 'Wednesday 2023-09-27 17:47 UTC - `gitlab-org/gitlab` broken `master` with rspec-ee system pg14 4/10' }
      let(:previous_incidents) { [double('Incident', web_url: "https://gitlab.com/group/project/-/issues/2", title: previous_incident_title)] }

      it 'returns nil' do
        expect(subject.duplicate_incident_url).to be_nil
      end
    end

    context 'when only the previous incident failed with the same jobs' do
      let(:previous_incident_title) { 'Wednesday 2023-09-27 17:47 UTC - `gitlab-org/gitlab` broken `master` with job 999' }
      let(:previous_incident) do
        double('Incident', web_url: "https://gitlab.com/group/project/-/issues/2", title: previous_incident_title, _links: {})
      end

      let(:previous_incidents) { [previous_incident] }

      it 'returns the previous incident url' do
        expect(subject.duplicate_incident_url).to eq(previous_incident.web_url)
      end
    end

    context 'when the previous incident failed with the same jobs and was closed as a duplicate of an earlier incident' do
      let(:previous_incident_title) { 'Wednesday 2023-09-27 17:47 UTC - `gitlab-org/gitlab` broken `master` with job 999' }
      let(:root_incident) do
        double('Root incident', web_url: "https://gitlab.example.com/group/project/issues/1", _links: { self: "https://gitlab.example.com/api/v4/projects/1/issues/1" })
      end

      let(:previous_incident) do
        double('Previous duplicate incident', title: previous_incident_title, _links: { 'closed_as_duplicate_of' => root_incident._links[:self] })
      end

      let(:previous_incidents) { [previous_incident] }

      it 'returns the url of the that earlier incident' do
        expect(Triage.api_client).to receive(:get).with('/projects/1/issues/1').and_return(root_incident)

        expect(subject.duplicate_incident_url).to eq(root_incident.web_url)
      end
    end
  end
end
